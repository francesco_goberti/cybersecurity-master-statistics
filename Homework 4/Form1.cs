﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Coin_Toss
{
    public partial class Form1 : Form
    {
        Bitmap b1;
        Graphics g1;
        Bitmap b2;
        Graphics g2;
        Bitmap b3;
        Graphics g3;
        Bitmap b4;
        Graphics g4;
        Random r = new Random();
        Pen PenTrajectory = new Pen(Color.GreenYellow, 1);

        
        
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            //Bitmap and graphic object creation
            b1 = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            g1 = Graphics.FromImage(b1);
            g1.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            b2 = new Bitmap(pictureBox3.Width, pictureBox3.Height);
            g2 = Graphics.FromImage(b2);
            g2.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            b3 = new Bitmap(pictureBox5.Width, pictureBox5.Height);
            g3 = Graphics.FromImage(b3);
            g3.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            b4 = new Bitmap(pictureBox5.Width, pictureBox5.Height);
            g4 = Graphics.FromImage(b4);
            g4.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        public int linearTransformX(double X, double minX, double maxX, int W)
        {
            return (int)(W * ((X - minX) / (maxX - minX)));
        }

        public int linearTransformY(double Y, double minY, double maxY, int H)
        {
            return (int)(H - H * ((Y - minY) / (maxY - minY)));
        }
        public List<Point> lastAbsolutePoints = new List<Point>();
        public List<Point> lastRelativePoints = new List<Point>();
        public List<Point> lastNormPoints = new List<Point>();
        public int stop = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            stop++;
            timer1.Enabled = true;
            button1.Enabled = false;
            if (stop == 100) {
                timer1.Enabled = false;
            }



            //number of trials and prob set from trackbars
            int trialsCount = trackBar1.Value;
            double probOfSucc = (double)trackBar2.Value / 100;

            //min and max values set for the conversion to graphic coordinates
            double minX = 0;
            double maxX = (double)trialsCount;
            double minY = 0;
            double maxY = (double)trialsCount;


            

            //number of successes
            int Y = 0;
            //points list
            Point[] absolutePoints = new Point[trialsCount];
            Point[] relativePoints = new Point[trialsCount];
            Point[] normPoints = new Point[trialsCount];

            

            double Yt1 = 0;
            double Yt2 = 0;

            for (int x = 0; x < trialsCount; x++) {
                double trial = r.NextDouble();
                if (trial < probOfSucc) {
                    Y++;
                    Yt1++;
                    Yt2++;
                    
                }


                int AbsoluteXCord = linearTransformX(x, minX, maxX,  pictureBox1.Width);
                int AbsoluteYCord = linearTransformY(Y,minY, maxY, pictureBox1.Height);
                
                Point absolutePoint = new Point(AbsoluteXCord, AbsoluteYCord);
                absolutePoints[x] = absolutePoint;

                double relativeY = Yt1 / (x + 1);
                int relativeXCord =  linearTransformX(x, minX, maxX, pictureBox3.Width);
                int relativeYCord =  linearTransformY(relativeY,minY, 1, pictureBox3.Height);
                Point relativePoint = new Point(relativeXCord, relativeYCord);
                relativePoints[x] = relativePoint;

                double normY = Yt2 / Math.Sqrt(x + 1);
                int normXCord = linearTransformX(x, minX, maxX, pictureBox5.Width);
                int normYCord = linearTransformY(normY, minY, maxY*probOfSucc,pictureBox5.Height);
                Point normPoint = new Point(normXCord, normYCord);
                normPoints[x] = normPoint;

                if (x == trialsCount - 1)
                {
                    lastAbsolutePoints.Add(absolutePoint);
                    lastRelativePoints.Add(relativePoint);
                    lastNormPoints.Add(normPoint);
                }
            }

            g1.DrawLines(PenTrajectory, absolutePoints.ToArray());
            g2.DrawLines(PenTrajectory,relativePoints.ToArray());
            g3.DrawLines(PenTrajectory, normPoints.ToArray());

            pictureBox1.Image = b1;
            pictureBox3.Image = b2;
            pictureBox5.Image = b3;



            int absmin_y = lastAbsolutePoints.Min(p => p.Y);
            int absmax_y = lastAbsolutePoints.Max(p => p.Y);
            int totalDistribution = absmax_y - absmin_y;
            int nIntervals = 5;
            int intervalSize = totalDistribution / nIntervals;
            int[] distances = new int[nIntervals];
            for (int i = 0; i < nIntervals; i++) {
                distances[i] = intervalSize * (i + 1);
            }
            List<Rectangle> absChart = new List<Rectangle>();
            for (int i = 0; i < nIntervals; i++) {
                absChart.Add(new Rectangle(0, distances[i] + absmin_y, 10, nIntervals));
            }

            int maxDist = pictureBox2.Right - pictureBox2.Left - 20;

            
            absChart.ToArray();
            foreach (Point p in lastAbsolutePoints) {

                richTextBox1.AppendText(lastAbsolutePoints.ToArray().Length.ToString() +  " " + (absmin_y + intervalSize).ToString() + " " + p.Y.ToString() + "\n");
                
                
            }


            foreach (Rectangle rect in absChart)
            {
                g4.FillRectangle(Brushes.Red, rect);
                g4.DrawRectangle(Pens.Red, rect);
            }


            pictureBox2.Image = b4;

        }
        
        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            button1.Enabled = true;
            textBox1.Text = trackBar1.Value.ToString();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            double v = (double)trackBar2.Value / 100;
            textBox2.Text = v.ToString();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }


    }
}
