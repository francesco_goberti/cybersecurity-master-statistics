﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Wireshark_Parser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public class Wireshark {
            public string num;
            public string time;
            public string src;
            public string dst;
            public string protocol;
            public string length;
            public string info;
        }

        List<Wireshark> w_list = new List<Wireshark> ();
        Dictionary<string, double> sources = new Dictionary<string, double>();
        Dictionary<string, double> destinations = new Dictionary<string, double>();


        private void Form1_Load(object sender, EventArgs e)
                {

                }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = true;
            using (var sr = new StreamReader(@"C:\Users\Francesco\Desktop\Sapienza\AA2223\Statistics\wireshark_sapienza.csv"))
            {
                
                var raw_header = sr.ReadLine();
                var header = raw_header.Split(',');
                foreach (var s in header) {
                    dataGridView1.Columns.Add(s.ToString().Trim('"'),s.ToString().Trim('"'));
                }


                int x = 0;
                while (x < 1000)
                {
                    var line = sr.ReadLine();
                    var value = line.Split(',');
/*
                    for (int i = 0; i < value.Length; i++)
                    {
                        value[i] = value[i].Trim('"'); 
                    }
*/
                    Wireshark wireshark = new Wireshark();
                    wireshark.num = value[0];
                    wireshark.time = value[1];
                    wireshark.src = value[2];
                    wireshark.dst = value[3];
                    wireshark.protocol = value[4];
                    wireshark.length =  value[5];
                    wireshark.info = value[6];

                    if (sources.ContainsKey(wireshark.src))
                    {
                        sources[wireshark.src] += 1;
                    }
                    else {
                        sources.Add(wireshark.src, 1);
                    }
                    if (destinations.ContainsKey(wireshark.dst))
                    {
                        destinations[wireshark.dst] += 1;
                    }
                    else
                    {
                        destinations.Add(wireshark.dst, 1);
                    }

                    dataGridView1.Rows.Add(value);
                    w_list.Add(wireshark);
                    x++;
                }

                foreach (KeyValuePair<string, double> entry in sources)
                {
                    richTextBox1.AppendText(entry.Key.ToString() + " " + entry.Value.ToString() + " -> " + ((entry.Value/1000)*100).ToString() + "%\n");
                    
                }
                richTextBox1.AppendText("\n");
                foreach (KeyValuePair<string, double> entry in destinations)
                {
                    richTextBox1.AppendText(entry.Key.ToString() + " " + entry.Value.ToString() + " -> " + ((entry.Value / 1000) * 100).ToString() + "%\n");

                }


            }
            button1.Enabled = false;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
