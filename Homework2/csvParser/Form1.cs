using Microsoft.VisualBasic.FileIO;
using System.Data;

namespace CSVparser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        class Student
        {
            public string name;
            public string sex;
            public double height;
            public double weight;
            public string hair_color;
            public string eye_color;
            public double age;
            public double shoe_size;
            public double nsiblings;
            public double ncars;
            public string hobby;
            public string smoker;
            public double npets;
            public string work;
            public double favnum;

        }

        public double nmale = 0;
        public double nfemale = 0;
        public int nstud = 0;
        public double nbrown = 0;
        public double nblue = 0;
        public double ngreen = 0;
        public double nblack = 0;
        List<Student> students = new List<Student>();

        

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

            using (var sr = new StreamReader(@"D:\statistics_students.csv")) {
 
                var raw_header = sr.ReadLine();

                while (!sr.EndOfStream)
                {
                    var line  = sr.ReadLine();
                    var value = line.Split(',');
                    nstud++;

                    Student student = new Student();

                    if (value[0].Length > 1) student.name = value[0]; else student.name = "Null";
                    if (value[1].Length > 1) student.sex = value[1]; else student.sex = "Other";
                    if (value[2].Length > 1) student.weight = Convert.ToDouble(value[2]); else student.weight = 0;
                    if (value[3].Length > 1) student.height = Convert.ToDouble(value[3]); else student.height = 0;
                    if (value[4].Length > 1) student.hair_color = value[4]; else student.hair_color = "Null";
                    if (value[5].Length > 1) student.eye_color = value[5]; else student.eye_color = "Null";
                    if (value[6].Length > 1) student.age = Convert.ToDouble(value[6]); else student.age = 0;
                    if (value[7].Length > 1) student.shoe_size = Convert.ToDouble(value[7]); else student.shoe_size = 0;
                    if (value[8].Length > 1) student.nsiblings = Convert.ToDouble(value[8]); else student.nsiblings = 0;
                    if (value[9].Length > 1) student.ncars = Convert.ToDouble(value[9]); else student.ncars = 0;
                    if (value[10].Length > 1) student.hobby = value[10]; else student.hobby = "Null";
                    if (value[11].Length > 1) student.smoker = value[11]; else student.smoker = "Null";
                    if (value[12].Length > 1) student.npets = Convert.ToDouble(value[12]); else student.npets = 0;
                    if (value[13].Length > 1) student.work = value[13]; else student.work = "Null";
                    if (value[14].Length > 1) student.favnum = Convert.ToDouble(value[14]); else student.favnum = 0;

                    students.Add(student);

                }
                richTextBox1.AppendText("Name".PadRight(45) + "Sex".PadRight(30) + "Weight".PadRight(30) + "Height".PadRight(30) + "HairColor".PadRight(30) + "\n");
                foreach (Student s in students)
                {
                    richTextBox1.AppendText(s.name.PadRight(45) + s.sex.PadRight(30) + s.weight.ToString().PadRight(30) + s.height.ToString().PadRight(30) + s.hair_color.PadRight(30) + "\n");
                    if (s.sex.ToLower() == "male")
                    {
                        nmale++;
                    }
                    else
                    {
                        nfemale++;
                    }
                    if (s.hair_color.ToLower() == "brown")
                    {
                        nbrown++;
                    }
                    else if (s.hair_color.ToLower() == "blue")
                    {
                        nblue++;
                    }
                    else if (s.hair_color.ToLower() == "green")
                    {
                        ngreen++;
                    }
                    else
                    {
                        nblack++;
                            
                    }
                }

            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {
            richTextBox2.AppendText("number of students: " + nstud.ToString() + "\n"
                                    + "male: " + nmale.ToString() + " " + ((nmale/nstud)*100).ToString() +"%\n"
                                    + "female: " + nfemale.ToString() + " "+ ((nfemale / nstud) * 100).ToString() + "%\n" 
                                    + "brown hair: " + nbrown.ToString() + " "+ ((nbrown/nstud)*100).ToString() +"%\n"
                                    + "blue hair: " + nblue.ToString() + " "+ ((nblue/ nstud) * 100).ToString() + "%\n"
                                    + "green hair: " + ngreen.ToString() + " "+ ((ngreen / nstud) * 100).ToString() + "%\n"
                                    + "black hair: " + nblack.ToString() + " "+ ((nblack / nstud) * 100).ToString() + "%\n"
                );
        }
    }
}