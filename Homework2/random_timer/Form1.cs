using System.Drawing.Text;

namespace homework2
{
    public partial class Form1 : Form
    {
        public double total = 0;
        public double time = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void start(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            time += 1;
            var r = new Random();
            double num = r.NextDouble();
            total += num;
            richTextBox1.AppendText(num.ToString() + " "  + "\n");
            richTextBox2.ResetText();
            richTextBox2.AppendText((total / time).ToString());
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}