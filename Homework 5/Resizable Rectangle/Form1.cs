﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoveResizeZoom_Rectangle
{
    public partial class Form1 : Form
    {
        ResizableRectangle rr;
        Bitmap b;
        Graphics g;
        public Form1()
        {
            InitializeComponent();
            b = new Bitmap(pictureBox2.Width, pictureBox2.Height);
            g = Graphics.FromImage(b);
            
            rr = new ResizableRectangle(10, 10, 200, 100, pictureBox2,this);

            g.DrawRectangle(Pens.Blue, rr.r);

            pictureBox2.Image = b;
            timer2.Start();


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            g.Clear(pictureBox2.BackColor);
            g.DrawRectangle(Pens.Blue, rr.r);
            

            pictureBox2.Image = b;
        }
    }
    
}
