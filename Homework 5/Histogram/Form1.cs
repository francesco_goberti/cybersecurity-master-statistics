﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Histogram
{
    public partial class Form1 : Form
    {
        Bitmap b;
        Graphics g;
        ResizableRectangle rr1;
        ResizableRectangle rr2;
        int[] dist = new int[10];
        public Form1()
        {
            InitializeComponent();

            b = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            g = Graphics.FromImage(b);
            rr1 = new ResizableRectangle(10, 10, 300, 250, pictureBox1, this);
            g.FillRectangle(Brushes.Black, rr1.r);
            g.DrawRectangle(Pens.Black, rr1.r);
            rr2 = new ResizableRectangle(370, 10, 300, 250, pictureBox1, this);
            g.FillRectangle(Brushes.Black, rr2.r);
            g.DrawRectangle(Pens.Black, rr2.r);

            pictureBox1.Image = b;
            timer1.Start();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        /*private void timer1_Tick(object sender, EventArgs e)
        {
            g.Clear(pictureBox1.BackColor);
            g.DrawRectangle(Pens.Black, rr1.r);
            g.FillRectangle(Brushes.Black, rr1.r);
            g.DrawRectangle(Pens.Black, rr2.r);
            g.FillRectangle(Brushes.Black, rr2.r);
            pictureBox1.Image = b;
        }*/

        private void horizontal_histogram() { 
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //distribution generation
            Random rand = new Random();
            int Y = 0;
            Dictionary<int, int> intervals = new Dictionary<int, int>();
            
            for (int i = 0; i < 1000; i++) {
                Y = 0;
                for (int j = 0; j < 100; j++) {
                    if (rand.NextDouble() < 0.5) {
                        Y++;
                    }
                }
                dist[Y / 10]++;

            }
            //---------------------------------------------
            int maxValue = dist.Max();

            int rectHeigth = rr1.r.Width - 20;
            int rectWidth = rr1.r.Height - 20;

            int nIntervals = dist.Length;
            int singleGraphWidth = rectWidth / nIntervals;
            int start = rr1.r.X;

            foreach (int i in dist) { 
                int singleGraphHeigth = (int)(((double)i/ (double)maxValue)*(double)rectHeigth);
                Rectangle rect = new Rectangle(start, rr1.r.Bottom - singleGraphHeigth, singleGraphWidth, singleGraphHeigth);
                g.FillRectangle(Brushes.Green, rect);
                g.DrawRectangle(Pens.White, rect);
                start += singleGraphWidth;
            }

            pictureBox1.Image = b;
            
        }
        private void orizontalHistogram() {
            int maxValue = dist.Max();

            int rectHeigth = rr1.r.Width - 20;
            int rectWidth = rr1.r.Height - 20;

            int nIntervals = dist.Length;
            int singleGraphWidth = rectWidth / nIntervals;
            int start = rr1.r.X;

            foreach (int i in dist)
            {
                int singleGraphHeigth = (int)(((double)i / (double)maxValue) * (double)rectHeigth);
                Rectangle rect = new Rectangle(start, rr1.r.Bottom - singleGraphHeigth, singleGraphWidth, singleGraphHeigth);
                g.FillRectangle(Brushes.Green, rect);
                g.DrawRectangle(Pens.White, rect);
                start += singleGraphWidth;
            }

            pictureBox1.Image = b;
        }

        private void verticalHistogram()
        {
            int maxValue = dist.Max();

            int rectHeigth = rr2.r.Width - 20;
            int rectWidth = rr2.r.Height - 20;

            int nIntervals = dist.Length;
            int singleGraphWidth = rectHeigth / nIntervals;
            int start = rr2.r.Y;

            foreach (int i in dist)
            {
                int singleGraphHeigth = (int)(((double)i / (double)maxValue) * (double)rectWidth);
                Rectangle rect = new Rectangle(rr2.r.Left, start, singleGraphHeigth, singleGraphWidth);
                g.FillRectangle(Brushes.Green, rect);
                g.DrawRectangle(Pens.White, rect);
                start += singleGraphWidth;
            }

            pictureBox1.Image = b;
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            g.Clear(pictureBox1.BackColor);
            g.DrawRectangle(Pens.Black, rr1.r);
            g.FillRectangle(Brushes.Black, rr1.r);
            g.DrawRectangle(Pens.Black, rr2.r);
            g.FillRectangle(Brushes.Black, rr2.r);
            orizontalHistogram();
            verticalHistogram();
        }
    }
}
