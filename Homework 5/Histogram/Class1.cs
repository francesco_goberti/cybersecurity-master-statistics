﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Histogram
{
    class ResizableRectangle
    {
        public Rectangle r;
        public PictureBox p;
        public Form f;

        public ResizableRectangle(int X, int Y, int Width, int Heigth, PictureBox p, Form f)
        {
            r = new Rectangle(X, Y, Width, Heigth);
            this.p = p;
            this.f = f;

            this.p.MouseUp += new MouseEventHandler(mouse_up);
            this.p.MouseDown += new MouseEventHandler(mouse_down);
            this.p.MouseMove += new MouseEventHandler(mouse_move);
            this.f.MouseWheel += new MouseEventHandler(mouse_scroll);

        }

        int x_down;
        int y_down;

        int x_mouse;
        int y_mouse;

        int r_width;
        int r_height;


        bool drag = false;
        bool resizing = false;

        double scale = 0.1d;

        private void mouse_up(object sender, MouseEventArgs e)
        {
            drag = false;
            resizing = false;
        }
        //mouse down ------------------------------------------------
        private void mouse_down(object sender, MouseEventArgs e)
        {
            if (r.Contains(e.X, e.Y))
            {
                x_mouse = e.X;
                y_mouse = e.Y;

                x_down = r.X;
                y_down = r.Y;

                r_width = r.Width;
                r_height = r.Height;

                if (e.Button == MouseButtons.Left)
                {
                    drag = true;
                }
                else if (e.Button == MouseButtons.Right)
                {
                    resizing = true;
                }

            }
        }
        //-----------------------------------------------------------

        //mouse move ------------------------------------------------
        private void mouse_move(object sender, MouseEventArgs e)
        {
            int delta_x = e.X - x_mouse;
            int delta_y = e.Y - y_mouse;

            if (drag)
            {

                r.X = x_down + delta_x;
                r.Y = y_down + delta_y;


            }
            else if (resizing)
            {
                r.Width = r_width + delta_x;
                r.Height = r_height + delta_y;


            }



        }
        //--------------------------------------------------------------
        private void mouse_scroll(object sender, MouseEventArgs e)
        {

            if (r.Contains(e.X, e.Y))
            {
                x_down = r.X;
                y_down = r.Y;

                r.Width = r.Width + (int)(e.Delta * scale);
                r.Height = r.Height + (int)(e.Delta * scale);

                r.X = x_down - (int)((e.Delta * scale) / 2);
                r.Y = y_down - (int)((e.Delta * scale) / 2);
            }
        }
    }
}

