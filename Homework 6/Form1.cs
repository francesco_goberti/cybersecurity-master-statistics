using App_Histogram_C;
using App_Wshark_C;
using Microsoft.VisualBasic.FileIO;
using System.Collections.Generic;
using System.Drawing;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Numerics;
namespace Homework_6
{
    public partial class Form1 : Form
    {
        TextFieldParser? myParser = null;
        List<Packet>? mySet = null;
        Rectangle r;
        Rectangle r2;
        
        int num_samples = 100;
        int sample_size = 10;
        int[][] all_samples;
        double[] means;
        double[] variances;
        int maxPacketLength = 0;
        Dictionary<Interval, int> dist_mean;
        Dictionary<Interval, int> dist_variances;
        Random rand = new Random();
        EditableRectangle rr1;
        EditableRectangle rr2;
        Bitmap b;
        Graphics g;
        BigInteger tot;
        public Form1()
        {
            InitializeComponent();
            compute_distribution();
        }
        private void compute_distribution()
        {
            if (myParser != null)
            {
                myParser.Close();
            }

            myParser = new TextFieldParser(Path.Combine(Directory.GetCurrentDirectory(), Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName + "\\packets.csv"));
            myParser.Delimiters = new string[] { "," };

            myParser.ReadFields(); // reading the first line

            mySet = new List<Packet>();

            while (!myParser.EndOfData)
            {
                string[] currentrow = myParser.ReadFields();

                for (int i = 0; i < currentrow.Length; i++)
                {
                    currentrow[i] = currentrow[i].Trim('"');
                }

                int no = Convert.ToInt32(currentrow[0]);
                double t = Convert.ToDouble(currentrow[1]);
                string sa = Convert.ToString(currentrow[2]);
                string da = Convert.ToString(currentrow[3]);
                string p = Convert.ToString(currentrow[4]);
                int len = Convert.ToInt32(currentrow[5]);
                string inf = Convert.ToString(currentrow[6]);

                Packet pack = new Packet(no, t, sa, da, p, len, inf);
                if (pack.length > maxPacketLength) { 
                    maxPacketLength = pack.length;
                }
                mySet.Add(pack);
            }
            totalDistributions();
            


        }
        public void totalDistributions()
        {
            int total = 0;
            foreach (Packet p in mySet) {
                total += p.length;
            }
            int mean = total / mySet.Count;
            int variance = 0;
            tot = new BigInteger(0);
            foreach (Packet p in mySet)
            {
                tot += (int)Math.Pow(p.length - mean,2);
            }
            variance = (int)(tot/mySet.Count);
            richTextBox1.AppendText("MEAN OF THE TOTAL POPULTATION: " + mean.ToString()+"\n");
            richTextBox1.AppendText("VARIANCE OF THE TOTAL POPULTATION: " + variance.ToString() + "\n\n");
            /*
            Dictionary<Interval, int> dict = new Dictionary<Interval, int>();
            int maxValue = maxPacketLength;
            int max_value = 64;
            Interval i_0 = new Interval(0, max_value);

            dict[i_0] = 0;

            foreach (Packet p in mySet)
            {
                bool inserted = false;
                List<Interval> list = dict.Keys.ToList();

                int lunghezza = p.length;

                foreach (Interval i in list)
                {
                    if (lunghezza >= i.down && lunghezza <= i.up)
                    {
                        dict[i]++;
                        inserted = true;
                        break;
                    }

                }
                while (!inserted)
                {
                    Interval i_x = new Interval(max_value + 1, max_value * 2);
                    if (max_value > maxValue) {
                        break;
                    }
                    max_value = max_value * 2;

                    dict[i_x] = 0;

                    if (lunghezza >= i_x.down && lunghezza <= i_x.up)
                    {
                        dict[i_x]++;
                        inserted = true;
                    }
                }
            }

            richTextBox1.AppendText("MEAN OF THE TOTAL DISTRIBUTION : " + (dict.Values.Sum() / dict.Count).ToString()+"\n");
        */
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            sample_size = trackBar1.Value;
            string LabelText = "Sample size -" + sample_size.ToString();
            label1.Text = LabelText;
            label3.Text = "Distribution of the means for " + num_samples.ToString() + " samples of size " + sample_size.ToString();
            label4.Text = "Distribution of the variances for " + num_samples.ToString() + " samples of size " + sample_size.ToString();
        }



        private void trackBar2_Scroll_1(object sender, EventArgs e)
        {
            num_samples = trackBar2.Value;
            string LabelText = "Number of samples -" + num_samples.ToString();
            label2.Text = LabelText;
            label3.Text = "Distribution of the means for " + num_samples.ToString() + " samples of size " + sample_size.ToString();
            label4.Text = "Distribution of the variances for " + num_samples.ToString() + " samples of size " + sample_size.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button2.Enabled = true;
            button1.Enabled = false;
            trackBar1.Enabled = false;
            trackBar2.Enabled = false;

            all_samples = new int[num_samples][];
            for (int i = 0; i < num_samples; i++)
            {

                int[] single_sample = new int[sample_size];
                for (int j = 0; j < sample_size; j++)
                {
                    int ri = rand.Next(0, mySet.Count);
                    Packet p = mySet[ri];
                    single_sample[j] = p.length;
                }

                all_samples[i] = single_sample;

            }
            int total;
            int mean;
            means = new double[num_samples];
            variances = new double[num_samples];
            for (int i = 0; i < num_samples; i++)
            {
                means[i] = compute_mean(all_samples[i]);
                variances[i] = compute_variance(all_samples[i]);
            }
           

        }

        private void button2_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            b = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            g = Graphics.FromImage(b);
            r = new Rectangle(20, 350, 450, 250);
            r2 = new Rectangle(600, 350, 450, 250);
            g.DrawRectangle(Pens.White, r);
            g.FillRectangle(Brushes.Black, r);
            g.DrawRectangle(Pens.White, r2);
            g.FillRectangle(Brushes.Black, r2);
            pictureBox1.Image = b;
            orizontalHistogramMean();
            orizontalHistogramVariance();
        }

        private void orizontalHistogramMean()
        {
            double maxValue = means.Max();
            int rectHeigth = r.Height-20;
            int rectWidth = r.Width -20;

            int nIntervals = 50;
            int singleGraphWidth = rectWidth / nIntervals;
            int start = r.X;
            Dictionary<Interval, int> dict = new Dictionary<Interval, int>();
            int max_value = (int)(maxValue/nIntervals);
            int span = max_value;
            Interval i_0 = new Interval(0, max_value);

            dict[i_0] = 0;
            List<Interval> list;
            int sum = 0;
            for (int i = 0; i < means.Length; i++)
            {

                sum += (int)means[i];
                bool inserted = false;
                list = dict.Keys.ToList();

                

                foreach (Interval j in list)
                {
                    if (means[i] >= j.down && means[i] <= j.up)
                    {
                        dict[j]++;
                        inserted = true;
                        break;
                    }

                }
                while (!inserted)
                {
                    
                    Interval i_x = new Interval(max_value + 1, max_value + span );
                    if(max_value > maxValue)
                    {
                        break;
                    }
                    max_value = max_value + span;

                    dict[i_x] = 0;

                    if (means[i] >= i_x.down && means[i] <= i_x.up)
                    {
                        dict[i_x]++;
                        inserted = true;
                    }
                }
            }
            int meanMean = sum / means.Length;
            
            richTextBox1.AppendText("MEAN OF THE DISTRIBUTION: " + meanMean.ToString()+"\n");

            int maxOcc = dict.Values.ToList().Max();
            int meanOfDist = 0;
            int count = 1;
            int total = 0;
            foreach (KeyValuePair<Interval, int> item in dict)

            {
                
                //richTextBox1.AppendText(item.Key.ToString() + " " +item.Value.ToString() + "\n");
                //int singleGraphHeigth = (int)(((double)item.Value / (double)maxValue) * (double)rectHeigth);
                int singleGraphHeigth = rectHeigth * item.Value / maxOcc;
                Rectangle rect = new Rectangle(start, r.Bottom - singleGraphHeigth, singleGraphWidth, singleGraphHeigth);
                g.FillRectangle(Brushes.Green, rect);
                g.DrawRectangle(Pens.White, rect);
                start += singleGraphWidth;
            }
            /*
            count = dict.Count;
            total = dict.Values.Sum();
            meanOfDist = total / count;
            richTextBox1.AppendText("MEAN OF THE DISTRIBUTION: " + meanOfDist.ToString() + "\n");
            */
            pictureBox1.Image = b;
        }

        private void orizontalHistogramVariance()
        {
            double maxValue = variances.Max();
            //richTextBox1.AppendText("MAX VALUE: " + ((int)maxValue).ToString() + "\n");
            int rectHeigth = r2.Height -20;
            int rectWidth = r2.Width - 20;

            int nIntervals = 50;
            int singleGraphWidth = rectWidth / nIntervals;
            int start = r2.X;
            Dictionary<Interval, int> dict = new Dictionary<Interval, int>();
            int max_value = (int)(maxValue / nIntervals);
            int span = max_value;
            Interval i_0 = new Interval(0, max_value);

            dict[i_0] = 0;
            List<Interval> list;
            tot = new BigInteger(0);
            int meanVariances = (int)variances.Sum() / variances.Length;
            for (int i = 0; i < variances.Length; i++)
            {
                tot += (int)Math.Pow((int)variances[i] - meanVariances,2);

                bool inserted = false;
                list = dict.Keys.ToList();



                foreach (Interval j in list)
                {
                    if (variances[i] >= j.down && variances[i] <= j.up)
                    {
                        dict[j]++;
                        inserted = true;
                        break;
                    }

                }
                while (!inserted)
                {

                    Interval i_x = new Interval(max_value + 1, max_value + span);
                    if (max_value > maxValue)
                    {
                        break;
                    }
                    max_value = max_value + span;

                    dict[i_x] = 0;

                    if (variances[i] >= i_x.down && variances[i] <= i_x.up)
                    {
                        dict[i_x]++;
                        inserted = true;
                    }
                }
            }
            int varianceVariances = (int)(tot / variances.Length);

            richTextBox1.AppendText("VARIANCE OF THE DISTRIBUTION: " + varianceVariances.ToString() + "\n");


            int maxOcc = dict.Values.ToList().Max();
            foreach (KeyValuePair<Interval, int> item in dict)

            {

                //richTextBox1.AppendText(item.Key.ToString() + " " + item.Value.ToString() + "\n");
                //int singleGraphHeigth = (int)(((double)item.Value / (double)maxValue) * (double)rectHeigth);
                int singleGraphHeigth = rectHeigth*item.Value/maxOcc;
                Rectangle rect = new Rectangle(start, r2.Bottom - singleGraphHeigth, singleGraphWidth, singleGraphHeigth);
                g.FillRectangle(Brushes.Green, rect);
                g.DrawRectangle(Pens.White, rect);
                start += singleGraphWidth;
            }

            pictureBox1.Image = b;
        }


        public double compute_mean(int[] numbers) {
            int count = 1;
            double mean = 0;
            int size = numbers.Length;
            for (int i = 0; i < size; i++) { 
                mean = mean +((double)numbers[i] - mean)/(double)count;
                count++;
            }
            return mean;
        }
        public double compute_variance(int[] numbers) {
            double mean = compute_mean(numbers);
            double variance = 0;
            int size = numbers.Length;
            for (int i = 0; i < size; i++) {
                variance = variance + ((numbers[i] - mean) * (numbers[i] - mean));
            }
            variance = variance / (double)size;
            return variance;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            
        }

    }
}